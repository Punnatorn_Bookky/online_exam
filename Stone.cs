﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stone : MonoBehaviour
{

    public Route currentRoute;
    int routePosition;
    public int steps;
    bool isMoving;
    public static Stone Ins;
    public static GameObject Text1, Text2, Text3, Text4, Text5, Text6;

    public void Awake()
    {
        Ins = this;
    }
    void Start()
    {
        Manager._instance.Get();


    }
    public void Update()
    {


        if (Input.GetKeyDown(KeyCode.Space) && !isMoving)
        {
            steps = Random.Range(1, 7);
            Debug.Log("Dice Rolled" + steps);

            if (routePosition + steps < currentRoute.childNodeList.Count)
            {
                StartCoroutine(Move());
            }
            else
            {
                Debug.Log("Rolled Number is to high");
            }
            
        }
        
        IEnumerator Move()
        {
            if (isMoving)
            {
                yield break;
            }
            isMoving = true;
            while (steps > 0)
            {
                Vector3 nextPos = currentRoute.childNodeList[routePosition + 1].position;
                while (MoveToNextNode(nextPos)) { yield return null; }

                yield return new WaitForSeconds(0.1f);
                steps--;
                routePosition++;
            }


            isMoving = false;
        }

        bool MoveToNextNode(Vector3 goal)
        {
            return goal != (transform.position = Vector3.MoveTowards(transform.position, goal, 2f * Time.deltaTime));

        }
    }
    public void Text()
    {
        if (steps == 1)
        {
            Text1 = GameObject.Find("1");
            Text1.gameObject.SetActive(true);
        }
        if (steps == 2)
        {
            Text2 = GameObject.Find("2");
            Text2.gameObject.SetActive(true);
        }
        if (steps == 3)
        {
            Text3 = GameObject.Find("3");
            Text3.gameObject.SetActive(true);
        }
        if (steps == 4)
        {
            Text4 = GameObject.Find("4");
            Text4.gameObject.SetActive(true);
        }
        if (steps == 5)
        {
            Text5 = GameObject.Find("5");
            Text5.gameObject.SetActive(true);
        }
        if (steps == 6)
        {
            Text6 = GameObject.Find("6");
            Text6.gameObject.SetActive(true);
        }

    }
}
