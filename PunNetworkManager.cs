﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Pun.UtilityScripts;

public class PunNetworkManager : ConnectAndJoinRandom
{
    public static PunNetworkManager singleton;

    

   [Header("Spawn Info")]
    [Tooltip("The prefab to use for representing the player")]
    public GameObject GamePlayerPrefab;

    private void Awake() 
    { 
        singleton = this;

        
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        
        if (PunUserNetControl.LocalPlayerInstance == null)
        {
            Debug.Log("We are Instantiating LocalPlayer from" + SceneManagerHelper.ActiveSceneName);
            PunNetworkManager.singleton.SpawnPlayer();
        }
        else
        {
            Debug.Log("Ignoring scene load for" + SceneManagerHelper.ActiveSceneName);
        }


        Stone.Ins.Update();

    }

   
    public void SpawnPlayer()
    {
        PhotonNetwork.Instantiate(GamePlayerPrefab.name, new Vector3(0f,0f,0f), Quaternion.identity,0);
    }
    
    [PunRPC]
    void RecievedText()
    {
        Stone.Ins.Text();
        Debug.Log("RecievedText");
    }
    
}
